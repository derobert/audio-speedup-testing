TEMPO    := 1.4
PERCENT  := $(shell echo "$(TEMPO) 1 - 100 * p" | dc)

INFILES := $(wildcard clips/*.wav)

OUTPUT := $(patsubst clips/%.wav,out/%-ff-rb-def.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-ff-rb-short.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-rb-def.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-rb-short.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-rb-c6.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-sonic-def.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-sonic-c.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-soundstretch-def.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-soundstretch-speech.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-sox-music.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-cli-sox-speech.wav,$(INFILES))
OUTPUT += $(patsubst clips/%.wav,out/%-audacity-tempo-sbsms.wav,$(INFILES))

all: $(OUTPUT)
	for f in $(INFILES) $(OUTPUT); do printf '%s\t%0.2f seconds\n' "$$(basename "$$f")" "$$(ffprobe  -loglevel error -hide_banner -of default=nw=1:nk=1 -show_entries 'format=duration' "$$f")"; done | column -nt -s$$'\t'

clean:
	rm -f $(OUTPUT)

tmp/%-mono.wav: clips/%.wav
	@mkdir -p tmp
	sox "$<" "$@" channels 1

out/%.wav: clips/%.$(TEMPO).wav.PREPROC
	@echo "Cheating and using pre-processed for $@ (needs manual processing)"
	cp "$<" "$@"

out/%-ff-rb-def.wav: clips/%.wav
	@mkdir -p out
	ffmpeg -i "$<" -filter:a rubberband=tempo=$(TEMPO):pitchq=quality -y "$@"

out/%-ff-rb-short.wav: clips/%.wav
	@mkdir -p out
	@rm -f "$@"
	ffmpeg -i "$<" -filter:a rubberband=tempo=$(TEMPO):pitchq=quality:window=short -y "$@"

out/%-cli-rb-def.wav: clips/%.wav
	@mkdir -p out
	rubberband --tempo $(TEMPO) "$<" "$@"

out/%-cli-rb-short.wav: clips/%.wav
	@mkdir -p out
	rubberband --tempo $(TEMPO) --window-short "$<" "$@"

out/%-cli-rb-c6.wav: clips/%.wav
	@mkdir -p out
	rubberband --tempo $(TEMPO) --crisp 6 "$<" "$@"

out/%-cli-sonic-def.wav: tmp/%-mono.wav
	@mkdir -p out
	sonic -s $(TEMPO) "$<" "$@"

out/%-cli-sonic-c.wav: tmp/%-mono.wav
	@mkdir -p out
	sonic -c -s $(TEMPO) "$<" "$@"

out/%-cli-soundstretch-def.wav: clips/%.wav
	@mkdir -p out
	soundstretch "$<" "$@" -tempo=$(PERCENT)

out/%-cli-soundstretch-speech.wav: clips/%.wav
	@mkdir -p out
	soundstretch "$<" "$@" -tempo=$(PERCENT) -speech 

out/%-cli-sox-music.wav: clips/%.wav
	@mkdir -p out
	sox "$<" "$@" tempo -m $(TEMPO)

out/%-cli-sox-speech.wav: clips/%.wav
	@mkdir -p out
	sox "$<" "$@" tempo -s $(TEMPO)


.PHONY: all clean
